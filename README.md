# Duet3D Python Library

Simple Python client library for the Duet3D controller board.
This library uses the same API as the web service.
It allows the get status nd the running of GCode Commands.

```python
from duet3d.client import Client
client = Client("http://workbee.example.com")
status = client.get_status()
# assert status == {'key1': 'value1'}
result = client.run_move("X10")
# assert result == {"key2": "value2"}
```

## Development

```bash
poetry build
twine upload -r local dist/*
```
