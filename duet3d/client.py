"""Client Class to interface to a Duet3D controller board

Author: munnox.
"""
import datetime
import json
import logging
from typing import Any, Dict
from urllib.parse import quote, urljoin, urlparse

import requests
from requests.sessions import Session

from .errors import D3DConnectionError, D3DError
from .status import Status
from .utility import XYZCoord, make_coord

logger = logging.getLogger(__name__)


class BasicPrefixSession(Session):  # pylint: disable=too-few-public-methods
    """Basic Session to allow a url prefix to be maintained."""

    def __init__(self, *args, prefix_url=None, timeout=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.prefix_url = prefix_url
        self.timeout = timeout
        self.auth = kwargs.get("auth", None)

    # pylint: disable=missing-function-docstring
    def request(self, method, url, *args, **kwargs):
        url = urljoin(self.prefix_url, url)
        if (self.auth is not None) and ("auth" not in kwargs):
            kwargs["auth"] = self.auth

        if (self.timeout is not None) and ("timeout" not in kwargs):
            kwargs["timeout"] = self.timeout
        return super().request(method, url, *args, **kwargs)


class Client:
    """Duet3D Client for external Web Interface.


    >>> client = Client("http://workbee.example.com")
    >>> status = client.get_status()
    >>> z = status["coords"]["machine"][2]
    >>> client.run_macro("/macros/Z_probe.g")
    >>> client.run_move("Z5")

    """

    url: str
    status: Dict[str, Any]

    # Define the Work Coordinate System and the GCODE command to choose and to set them
    _wcsmap = {
        1: ["G54", "G10 L2 P1"],
        2: ["G55", "G10 L2 P2"],
        3: ["G56", "G10 L2 P3"],
        4: ["G57", "G10 L2 P4"],
        5: ["G58", "G10 L2 P5"],
        6: ["G59", "G10 L2 P6"],
        7: ["G59.1", "G10 L2 P7"],
        8: ["G59.2", "G10 L2 P8"],
        9: ["G59.3", "G10 L2 P9"],
    }

    def __init__(self, url: str, **kwargs):
        """Create the Duet3D Client using the given base url for the machine."""
        try:
            urlparse(url)
        except Exception as error:
            error_msg = (
                f"Invalid URL given to client: {url}. Error ({type(url)}): {url}",
            )
            logger.error(error_msg)
            raise D3DError(error_msg) from error
        if url.endswith("/"):
            raise D3DError(f"URL must not end with a slash. Given: {url}")
        self.url = url
        self.status = {}
        self.session = BasicPrefixSession(prefix_url=url, **kwargs)

    def get_status(self, type=3):
        """Get the Status of the Machine."""
        url = f"/rr_status?type={type}"
        # url = urljoin(self.url, url)
        response = self._get_wrap(
            url, "Status", headers={"Accept": "application/json, text/plain, */*"}
        )
        if "results" in response:
            return Status(response["results"][0])
        else:
            logger.error(
                "Machine status failed response from %s status %s", self.url, response
            )
            raise D3DError(f"Failed to get matchine status: {response}")

    def upload(self, filepath, data):
        """Upload Gcode."""
        filepath = quote(filepath)
        time_str = quote(datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"))
        url = f"/rr_upload?name={filepath}&time={time_str}"
        # url = urljoin(self.url, url)
        response = self._post_wrap(
            url,
            "Upload",
            headers={"Accept": "application/json, text/plain, */*"},
            data=data,
        )
        if "results" in response:
            return response["results"][0]
        else:
            logger.error(
                "Machine upload gcode failed response from %s status %s",
                self.url,
                response,
            )
            raise D3DError(f"Failed to upload file: {response}")
        return response

    def downloads(self, filepath):
        """Download gcode."""
        filepath = quote(filepath)
        url = f"/rr_download?name={filepath}"
        # url = urljoin(self.url, url)
        response = self._get_wrap(
            url, "Download", headers={"Accept": "application/json, text/plain, */*"}
        )
        if "content" in response:
            return response["content"]
        else:
            logger.error(
                "Machine download gcode failed response from %s status %s",
                self.url,
                response,
            )
            raise D3DError(f"Failed to download file: {response}")
        return response

    def filelist(self, dir):
        """Get a filelist"""
        # http://workbee.munnox.com/rr_filelist?dir=0:/macros&first=0
        dir_str = quote(dir)
        url = f"/rr_filelist?dir={dir_str}&first=0"
        # url = urljoin(self.url, url)
        response = self._get_wrap(
            url, "File list", headers={"Accept": "application/json, text/plain, */*"}
        )
        if "results" in response:
            return response["results"][0]
        else:
            logger.error(
                "Machine filelist failed response from %s result %s", self.url, response
            )
            raise D3DError(f"Failed to get filelist: {response}")

    @staticmethod
    def parse(line):
        """Parse reply line."""
        if line == "":
            return None
        return json.loads(line)

    def reply(self):
        """Get gcode reply.

        Empties machine buffer of any results stored.
        """
        # http://workbee.munnox.com/rr_reply
        url = "/rr_reply"
        # url = urljoin(self.url, url)
        res = self._get_wrap(
            url, "Reply", headers={"Accept": "application/json, text/plain, */*"}
        )
        if "content" in res and "results" not in res:
            reply_lines = res["content"].decode("utf-8").strip("\n").split("\n")
            results = [Client.parse(line) for line in reply_lines]
            res["results"] = [result for result in results if result is not None]
            del res["content"]
            del res["error"]
        logger.debug("Reply from %s = %s", self.url, res)
        return res

    def _get_wrap(self, url: str, cmd_message: str, **kwargs):
        """A Internal wraper for a GET request for consistancy."""
        # pylint: disable=broad-except
        # print(full_url)
        try:
            resp = self.session.get(url, **kwargs)
        except requests.exceptions.ConnectionError as error:
            raise D3DConnectionError(
                f"Cannot connect to machine ({self.url}) to run {cmd_message} Command"
            ) from error
        except Exception as error:
            raise D3DError(
                f"Error while running {cmd_message} Command type: {type(error)}, error: {error}"
            ) from error
        # Retained as this is a good point for debugging
        # logger.debug(
        #     "GET request wrap from %s response code %s, content: %s",
        #     full_url,
        #     resp.status_code,
        #     resp.content,
        # )
        if resp.status_code == 200:
            try:
                return {"results": [resp.json()]}
            except Exception as error:
                return {"content": resp.content, "error": error}
        else:
            raise D3DError(
                f"{cmd_message} Request failed response ({resp.status_code}): {resp.content.decode('utf-8')}"
            )

    def use_work_coordinate_system(self, wcs):
        """Select the given Work Coordinate System"""
        cmd = self._wcsmap[wcs][0]
        result = self.run_gcode(cmd)
        # result["reply"] = self.reply()
        # result["cmd"] = cmd
        return result

    def set_work_coordinate_system(self, wcs, xyz: XYZCoord):
        """Set the chosen coordinate system to the given XYZ values.

        This sets the position to 0,0,0 if all three coords are given as the current possition.
        """
        cmd = f"{self._wcsmap[wcs][1]} {make_coord(xyz)}"
        result = self.run_gcode(cmd)
        # result["reply"] = self.reply()
        # result["cmd"] = cmd
        return result

    def _post_wrap(self, url: str, cmd_message: str, **kwargs):
        """A Internal wraper for a POST request for consistancy."""
        # pylint: disable=broad-except
        try:
            resp = self.session.post(url, **kwargs)
        except requests.exceptions.ConnectionError as error:
            raise D3DConnectionError(
                f"Cannot connect to machine ({self.url}) to run {cmd_message} Command"
            ) from error
        except Exception as error:
            raise D3DError(
                f"Error while running {cmd_message} Command type: {type(error)}, error: {error}"
            ) from error
        # Retained as this is a good point for debugging
        # logger.debug(
        #     "POST request wrap from %s response code %s, content: %s",
        #     full_url,
        #     resp.status_code,
        #     resp.content,
        # )
        if resp.status_code == 200:
            try:
                return {"results": [resp.json()]}
            except Exception as error:
                return {"content": resp.content, "error": error}
        else:
            raise D3DError(
                f"{cmd_message} Request failed response ({resp.status_code}): {resp.content.decode('utf-8')}"
            )

    def _run_gcode_cmd(self, command: str):
        """Run the given gcode command on the machine."""
        encoded_command = quote(command)
        url = f"/rr_gcode?gcode={encoded_command}"
        response = self._get_wrap(
            url, "GCode", headers={"Accept": "application/json, text/plain, */*"}
        )
        response["command"] = command
        return response

    def run_gcode(self, command: str):
        """Run the given gcode command on the machine."""
        response = self._run_gcode_cmd(command)
        if "results" in response:
            return response  # ["results"][0]
        else:
            logger.error(
                "Machine Gcode command failed response from %s result %s",
                self.url,
                response,
            )
            raise D3DError(f"Failed to run GCode CMD: {command} response: {response}")

    def run_move(self, xyz: XYZCoord):
        """Run the given G1 gcode linear move command on the machine."""
        command = f"G1 {make_coord(xyz)}"
        return self._run_gcode_cmd(command)

    def run_move_rapid(self, xyz: XYZCoord):
        """Run the given G0 gcode rapid move command on the machine."""
        command = f"G0 {make_coord(xyz)}"
        return self._run_gcode_cmd(command)

    def dwell_ms(self, period: int):
        """Run the given G4 gcode dwell (ms) command on the machine."""
        command = f"G4 P{period}"
        return self._run_gcode_cmd(command)

    def run_macro(self, filepath: str):
        """Run the given G98 gcode run macro command on the machine."""
        command = f'M98 P"{filepath}"'
        return self._run_gcode_cmd(command)

    def set_absolute(self):
        """Run the given G90 gcode command to set apsolute XYZ positioning on the machine."""
        command = "M90"
        return self._run_gcode_cmd(command)

    def set_relative(self):
        """Run the given G91 gcode command to set relative XYZ positioning on the machine."""
        command = "M91"
        return self._run_gcode_cmd(command)

    def set_position(self, xyz: XYZCoord):
        """Run the given G92 gcode command to set current XYZ position on the machine."""
        command = f"M92 {make_coord(xyz)}"
        return self._run_gcode_cmd(command)
