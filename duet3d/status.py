"""Client Class to interface to a Duet3D controller board

Author: munnox.
"""
import datetime
import logging

logger = logging.getLogger(__name__)


class Status:
    """Machine Status Class."""

    def __init__(self, raw):
        self.update_time = datetime.datetime.now()
        self._raw = raw

    def age(self):
        """To give the age of the current status data."""
        return (datetime.datetime.now() - self.update_time).total_seconds()

    @property
    def work_coordinate_system(self):
        """Get the current work coordinate system wcs."""
        work_coordinate_system = self._raw["coords"]["wpl"]
        return work_coordinate_system

    @property
    def status(self):
        """Get the current Busy/Idle status of the machine."""
        return self._raw["status"]

    @property
    def is_busy(self):
        """Boolean flag for when the machine is busy."""
        return self.status == "B"

    @property
    def is_idle(self):
        """Boolean flag for when the machine is idle."""
        return self.status == "I"

    @property
    def position(self):
        """Get the current work position.

        This is effected by the current selection of the wcs.
        """
        xyz = self._raw["coords"]["xyz"]
        return dict(zip(["x", "y", "z"], xyz))

    @property
    def absolute_position(self):
        """Absolute machine position."""
        xyz = self._raw["coords"]["machine"]
        return dict(zip(["x", "y", "z"], xyz))

    def __str__(self):
        """Create a string from the Status."""
        return f"Status(pos={self.position}, abs={self.absolute_position})"
