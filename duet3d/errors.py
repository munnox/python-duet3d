"""Errors which can be raised by the Library.

Author: munnox.
"""


class D3DError(Exception):
    """Base Error for issues arrising within library."""


class D3DConnectionError(D3DError):
    """Error Raised when connection cannot be made."""
