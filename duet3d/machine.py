"""Client Class to interface to a Duet3D controller board

Author: munnox.
"""
import logging
import math
import time

from duet3d.status import Status

from .client import Client

logger = logging.getLogger(__name__)


class Machine:
    """A Digital Twin Class of the Duet Controller."""

    client: Client
    status: Status

    def __init__(self, client: Client):
        self.client = client
        self.status = self.client.get_status()

    def update(self):
        """Update the machine status."""
        logger.debug("Updating the machines status")
        self.status = self.client.get_status()

    def use_work_coordinate_system(self, wcs):
        """Select the given Work Coordinate System"""
        logger.debug("Choose WCS.")
        result = self.client.use_work_coordinate_system(wcs)
        self.update()
        return result

    def set_work_coordinate_system(self, wcs, xyz):
        """Set the chosen coordinate system to the given XYZ values.

        This sets the position to 0,0,0 if all three coords are given as the current possition.
        """
        logger.debug("Set WCS to given Coordinate system %s.", xyz)
        result = self.client.set_work_coordinate_system(wcs, xyz)
        self.update()
        return result

    def wait(self, timeout, wait_period=0.1):
        """Block and Poll controller till idle."""
        start = time.time()
        tries = int(math.ceil(float(timeout) / wait_period))
        logger.debug("Running for %s Tries: %s", timeout, tries)

        for _ in range(tries):
            self.update()
            end = time.time()
            if self.status.is_idle:
                logger.debug("Found Idle took: %s", (end - start))
                return end - start
            if (end - start) >= timeout:
                raise Exception(
                    f"Wait time expired (elaspsed > timeout) : ({(end-start):0.3f} > {timeout}) ."
                )
            time.sleep(wait_period)
            # logger.debug("Iteration: %s waiting...", i)
        # End clause to ensure stopping of loop
        end = time.time()
        raise Exception(
            f"For loop expired (elaspsed > timeout) : ({(end-start):0.3f} > {timeout}) ."
        )

    @property
    def work_coordinate_system(self):
        """Get the current work coordinate system wcs."""
        return self.status.work_coordinate_system

    @property
    def position(self):
        """Get the current work position.

        This is effected by the current selection of the wcs.
        """
        return self.status.position

    @property
    def absolute_position(self):
        """Absolute machine position."""
        return self.status.absolute_position

    def __str__(self):
        """Create a string from the machine."""
        return (
            f"Machine(url={self.client.url}, "
            "wcs={self.work_coordinate_system}, "
            "pos={self.position}, "
            "abs={self.absolute_position})"
        )
