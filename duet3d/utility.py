"""Duet3D Client Library.

Author: munnox.
"""
__version__ = "0.0.1"

from typing import Dict, NewType

XYZCoord = NewType("XYZCoord", Dict[str, float])


def make_coord(xyz: XYZCoord) -> str:
    """Make a GCode string from a XYZ dict."""
    return " ".join((f"{k.upper()}{xyz[k]}" for k in xyz))
