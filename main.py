"""Example little program to run the client.

Author: munnox.
"""

import logging
import os
import sys

import dotenv

from duet3d.client import Client
from duet3d.machine import Machine

# from textwrap import dedent


logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stdout,
)
logger = logging.getLogger("main")
logging.getLogger("urllib3.connectionpool").setLevel(logging.WARN)

dotenv.load_dotenv()

url = os.getenv("URL", "http://workbee.example.com/")
client = Client(url)
status = client.get_status()
print("status: ", status)

result = client.run_gcode("M20 S2")
print(result)
# result = client.reply()
# print(result)
result = client.reply()
print(result)
result = client.run_gcode('M36 "/macros/test.g"')
print(result)
result = client.reply()
print(result)
result = client.reply()
print(result)
FILE_TO_UPLOAD = """
M20 s2; testing
;Testing
"""
result = client.upload(filepath="0:/macros/test1.g", data=FILE_TO_UPLOAD)
print(result)
result = client.downloads(filepath="0:/macros/test1.g")
print(result)
result = client.filelist(dir="0:/macros/")
print(result)

machine = Machine(client)
result = machine.use_work_coordinate_system(2)
print(machine, result)
result = machine.set_work_coordinate_system(3, machine.absolute_position)
print(machine, result)
result = machine.use_work_coordinate_system(3)
print(machine, result)


result = machine.client.run_macro(filepath="0:/macros/test1.g")
resulta = machine.client.reply()
print(machine, result, resulta)


ZPROBE = """M400 ;Wait for current moves to finish
G91 ; Relative Positioning
M563 P999 S"XYZ-Probe" ; Define or remove a tool Tool 999 called XYZ-Probe

T999 ; Choose Tool 999
M585 Z15 E3 L0 F500 S1 ; Probe Tool for 15 mm in Z WARNING E and L are only for RR V2
; Need to use P to select probe when using RR V3
T-1 ; Clear the tool
G10 L20 Z5 ; set position relative to current position
G1 Z5 F500 ; Move up 5 mm relative
M500 ; Save config parameters "sys/config-override.g"
G90 ; Set Absolute Positioning

M563 P999 D-1 H-1 ; Remove tool 999 seem this is a known cmd format
M291 P"Probe complete. Please remove probe." R"Success" S1 ; Show message to user
"""
result = client.upload(filepath="0:/macros/ram_z_probe.g", data=ZPROBE)
print(result)
# assert status == {'key1': 'value1'}
# result = client.run_move("X200")
# print(result)
# result = client.run_gcode("G0 X99 Y99")
# print(result)
# assert result == {"key2": "value2"}


# def probe_grid():
#     """Basic level grid probe with Z macro.

#     Place holder for now."""
#     # client = Duet3dClient("http://workbee.munnox.com")
#     Z_PROBE = dedent(
#         """M400 ;Wait for current moves to finish
#     G91 ; Relative Positioning
#     M563 P999 S"XYZ-Probe" ; Define or remove a tool Tool 999 called XYZ-Probe

#     T999 ; Choose Tool 999
#     M585 Z15 E3 L0 F500 S1 ; Probe Tool for 15 mm in Z WARNING E and L are only for RR V2
#     ; Need to use P to select probe when using RR V3
#     T-1 ; Clear the tool
#     G10 L20 Z5 ; set position relative to current position
#     G1 Z5 F500 ; Move up 5 mm relative
#     M500 ; Save config parameters "sys/config-override.g"
#     G90 ; Set Absolute Positioning

#     M563 P999 D-1 H-1 ; Remove tool 999 seem this is a known cmd format
#     M291 P"Probe complete. Please remove probe." R"Success" S1 ; Show message to user
#     ; Macro End with a Z=10
#     """
#     )
#     result = machine.client.upload(filepath="0:/macros/ram_z_probe.g", data=Z_PROBE)
#     print(result)

#     # import numpy as np

#     np.linspace(0, 5, 10)
#     result = machine.use_work_coordinate_system(2)

#     machine.update()
#     grid = []
#     for x in np.linspace(0, 100, 4):
#         row = []
#         for y in np.linspace(0, 100, 4):
#             pos = {"x": x, "y": y}
#             machine.client.run_move({"z": 15})
#             machine.client.run_move(pos)
#             machine.update()
#             # Z probe
#             result = machine.client.run_macro(filepath="0:/macros/ram_z_probe.g")
#             machine.update()
#             pos["z"] = machine.absolute_position["z"]
#             # Macro finished at Z=10 do this moved it up an extra 5 mm to Z =15 for the next move
#             machine.client.run_move({"z": 15})
#             row.append(pos)
#             # break;
#             time.sleep(2)
#         grid.append(row)
#         # break;

#     grid

#     # z = status['results'][0]["coords"]["machine"][2]
#     # z
#     # client.run_macro("/macros/Z_probe.g")
#     # clientfrom textwrap import dedent.run_move("Z5")
