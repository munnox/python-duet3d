"""Tests for the client.

Author: munnox.
"""
from urllib.parse import parse_qs, quote, urljoin, urlparse

import pytest
from dotenv import load_dotenv

from duet3d import __version__
from duet3d.client import BasicPrefixSession, Client
from duet3d.errors import D3DConnectionError, D3DError
from duet3d.status import Status
from duet3d.utility import make_coord

# Status on Workbee when just homed
MACHINE_STATUS = {
    "status": "I",
    "coords": {
        "axesHomed": [1, 1, 1],
        "wpl": 1,
        "xyz": [484.62, 475.04, 53.08],
        "machine": [550.0, 765.0, 94.0],
        "extr": [],
    },
    "speeds": {"requested": 0.0, "top": 0.0},
    "currentTool": -1,
    "params": {
        "atxPower": 0,
        "fanPercent": [0, 100, 0, 0, 0, 0, 0, 0, 0],
        "speedFactor": 100.0,
        "extrFactors": [],
        "babystep": 0.0,
    },
    "seq": 20,
    "sensors": {"probeValue": 0, "fanRPM": 0},
    "temps": {
        "current": [2000.0, 2000.0, 2000.0, 2000.0, 2000.0, 2000.0, 2000.0, 2000.0],
        "state": [0, 0, 0, 0, 0, 0, 0, 0],
        "tools": {"active": [], "standby": []},
        "extra": [{"name": "*MCU", "temp": 16.9}],
    },
    "time": 2790.0,
    "currentLayer": 0,
    "currentLayerTime": 0.0,
    "extrRaw": [],
    "fractionPrinted": 0.0,
    "headers": {"Accept": "application/json, text/plain, */*"},
    "filePosition": 0,
    "firstLayerDuration": 0.0,
    "firstLayerHeight": 0.0,
    "printDuration": 0.0,
    "warmUpDuration": 0.0,
    "timesLeft": {"file": 0.0, "filament": 0.0, "layer": 0.0},
}

load_dotenv()


def test_version():
    """Test library version."""
    assert __version__ == "0.0.1"


# This method will be used by the mock to replace requests.get
def mocked_requests(*args, **kwargs):
    """Create Mock response object for requests lib."""

    class MockResponse:
        """Test Mock Response Object."""

        def __init__(self, json_data, status_code):
            """Create mock response."""
            self.json_data = json_data
            self.status_code = status_code
            self.content = bytes(str(json_data), "utf-8")

        def json(self):
            """Get Mock JSON data."""
            return self.json_data

    request_url = urljoin(args[0].prefix_url, args[1])
    url = urlparse(request_url)
    params = parse_qs(url.query)

    print("url: ", url, params)
    if url.path == "/rr_status":
        return MockResponse({**MACHINE_STATUS, **kwargs}, 200)
    # elif args[0] == "http://workbee.example.com/rr_gcode?gcode=G1%20X10":
    #     parts = args[0].split("=")
    #     return MockResponse({"buff": 255 - len(parts[1]), **kwargs}, 200)
    elif url.path.find("/rr_gcode") >= 0:  # M36%20%22/macros/test.g%22"
        print("found")
        code = 200
        # Force a failure in responce to test 404
        if "G500" in params["gcode"]:
            code = 401
        print("Mocker:", params)
        return MockResponse(
            {"buff": 255 - len(quote(params["gcode"][0])), **kwargs}, code
        )

    return MockResponse(None, 404)


# monkeypatched requests.get moved to a fixture
@pytest.fixture
def mock_response(monkeypatch):
    """Requests.get() mocked to return {'mock_key':'mock_response'}."""

    def mock_session(*args, **kwargs):
        print("mock get/post args:", args, "kwargs: ", kwargs)
        return mocked_requests(*args, **kwargs)

    monkeypatch.setattr(BasicPrefixSession, "get", mock_session)
    monkeypatch.setattr(BasicPrefixSession, "post", mock_session)


# @mock.patch('requests.get', side_effect=mocked_requests_get)
def test_client_mock(mock_response):
    """Test Library against the mocked requests library when board is offline.
    To help with regression testing with potted responses."""
    # pylint: disable=unused-argument
    # pylint: disable=protected-access
    client = Client("http://workbee.example.com")
    status = client.get_status()
    assert status._raw == Status(MACHINE_STATUS)._raw
    result = client.run_move({"x": 10})
    assert result == {
        "command": "G1 X10",
        "results": [
            {
                "buff": 247,
                "headers": {"Accept": "application/json, text/plain, */*"},
            }
        ],
    }
    with pytest.raises(D3DError) as error:
        result = client.run_gcode("G500")
    assert "GCode Request failed response" in str(error.value)
    assert isinstance(error.value, D3DError)

    # pylint: disable=fixme
    # TODO Need to add mock tests for Upload, Download, Filelist, reply
    # These have been tested against the machine


def test_client_noconnection():
    """Test no connection error excpetion is raised"""
    # Assert that creating  client fail if a slash is left at the end
    with pytest.raises(D3DError) as error:
        client = Client("http://notworkbee.example.com/")
    assert "URL must not end with a slash." in str(error.value)

    client = Client("http://notworkbee.example.com")
    with pytest.raises(D3DConnectionError) as error:
        client.get_status()
    assert "Cannot connect to machine" in str(error.value)
    assert isinstance(error.value, D3DConnectionError)

    with pytest.raises(D3DConnectionError) as error:
        client.run_gcode("test")
    assert "Cannot connect to machine" in str(error.value)
    assert isinstance(error.value, D3DConnectionError)


def test_make_xyz():
    """Test no connection error excpetion is raised"""
    # Assert that creating  client fail if a slash is left at the end
    # client = Client("http://workbee.example.com")
    xyz = {"x": 56, "y": 34}
    res = make_coord(xyz)
    assert res == "X56 Y34"
